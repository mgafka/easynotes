<?php
/**
 * Created by PhpStorm.
 * User: mehow
 * Date: 15.12.17
 * Time: 12:35
 */

class Db
{
    private static $host = "localhost";
    private static $dbname = "easynotes";
    private static $username = "root";
    private static $password = "";

    private static $conn = null;

    public static function connect(){
        if (self::$conn == null){
            try {
                self::$conn = new PDO("mysql:host=".self::$host.";"."dbname=".self::$dbname, self::$username, self::$password);
            }
            catch (PDOException $exception){
                die($exception->getMessage());
            }
        }
        return self::$conn;
    }

    public static function disconnect(){
        self::$conn = null;
    }
}