<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="utf-8">
    <link   href="css/bootstrap.min.css" rel="stylesheet">
    <script src="js/bootstrap.min.js"></script>
</head>

<body>
    <div class="container">
            <div class="row">
                <h1>EasyNotes</h1>
            </div>
            <div class="row">
                <p>
                    <a href="create.php" class="btn btn-success">Add note</a>
                </p>
                <table class="table table-striped table-bordered">
                  <thead>
                    <tr>
                      <th>Note #</th>
                      <th>Content</th>
                      <th>Date</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php
                   include 'Db.php';
                   $pdo = Db::connect();
                   $sql = 'SELECT * FROM notes ORDER BY id';
                   foreach ($pdo->query($sql) as $row) {
                            echo '<tr>';
                            echo '<td>'. $row['id'] . '</td>';
                            echo '<td>'. $row['content'] . '</td>';
                            echo '<td>'. $row['date'] . '</td>';
                            echo '<td><a href="edit.php?id='.$row['id'].'" class="btn btn-success">Edit&nbsp; 
                            <a href="delete.php?id='.$row['id'].'" class="btn btn-danger">Delete</td>';
                            echo '</tr>';
                   }
                   Db::disconnect();
                  ?>
                  </tbody>
            </table>
        </div>
    </div> <!-- /container -->
  </body>
</html>