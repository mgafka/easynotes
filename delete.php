<?php
require 'Db.php';
    $id = $_GET['id'];

    //DELETE
if (isset($_POST['id'])) {
    $id2 = $_POST['id'];
    $db=Db::connect();
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $deletesql = "DELETE FROM notes WHERE id = ?";
    $delete = $db->prepare($deletesql);
    $delete->bindParam(1,$id2);
    $delete->execute();
    DB::disconnect();
    header("Location: index.php");
}

?>

<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="utf-8">
    <link   href="css/bootstrap.min.css" rel="stylesheet">
    <script src="js/bootstrap.min.js"></script>
</head>

<body>
<div class="container">
    <div class="row">
        <h1>EasyNotes</h1>
    </div>
    <div class="row">
        <h3>Delete note</h3>
        <form class="form-horizontal" action="delete.php" method="post">
            <input type="hidden" name="id" value="<?php echo $id;?>"/>
            <p class="alert alert-error">Are you sure to delete ?</p>
            <div class="form-actions">
                <button type="submit" class="btn btn-danger">Yes</button>
                <a class="btn" href="index.php">No</a>
            </div>
        </form>
    </div>
</div> <!-- /container -->
</body>
</html>

