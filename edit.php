<?php
require 'Db.php';
    $id = $_GET['id'];

    //SELECT
    $db = Db::connect();
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $select = "SELECT * FROM notes WHERE id = ?";
    $get = $db->prepare($select);
    $get->bindParam(1, $id,PDO::PARAM_STR);
    $get->execute();
    $data = $get->fetch(PDO::FETCH_ASSOC);
    $content = $data['content'];
    Db::disconnect();

    //UPDATE
    if (isset($_POST['newcontent'])) {
        $newcontent = $_POST['newcontent'];
        $editsql = "UPDATE notes SET content = ? WHERE id = ?";
        $edit = $db->prepare($editsql);
        $edit->bindParam(1, $newcontent, PDO::PARAM_STR);
        $edit->bindParam(2, $id, PDO::PARAM_INT);
        $edit->execute();
        Db::disconnect();
        header("Location: index.php");
    }

?>

<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="utf-8">
    <link   href="css/bootstrap.min.css" rel="stylesheet">
    <script src="js/bootstrap.min.js"></script>
</head>

<body>
<div class="container">
    <div class="row">
        <h1>EasyNotes</h1>
    </div>
    <div class="row">
        <h3>Edit note</h3>
        <form action="edit.php?id=<?php echo $id;?>" method="post">
            <input type="text" name="newcontent" placeholder="Content" required value="<?php echo $content;?>"/> <br />
            <button type="submit" class="btn btn-success">Edit</button>
            <a href="index.php" class="btn btn-danger">Back</a>
        </form>
    </div>
</div> <!-- /container -->
</body>
</html>

