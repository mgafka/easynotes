<?php
require 'Db.php';
if (isset($_POST['content'])){
    $content = $_POST['content'];
    $date = date("H:i d M Y");
    $db = Db::connect();
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $addquery = "INSERT INTO notes (content, date ) VALUES (?, ?)";
    $add = $db->prepare($addquery);
    $add->bindParam(1, $content,PDO::PARAM_STR);
    $add->bindParam(2, $date,PDO::PARAM_STR);
    $add->execute();
    Db::disconnect();
    header("Location: index.php");


}

?>

<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="utf-8">
    <link   href="css/bootstrap.min.css" rel="stylesheet">
    <script src="js/bootstrap.min.js"></script>
</head>

<body>
    <div class="container">
            <div class="row">
                <h1>EasyNotes</h1>
            </div>
            <div class="row">
                <h3>Add a new note</h3>
                <form action="create.php" method="post">
                    <input type="text" name="content" placeholder="Content" required/> <br />
                    <button type="submit" class="btn btn-success">Add</button>
                    <a href="index.php" class="btn btn-danger">Back</a>
                </form>
        </div>
    </div> <!-- /container -->
  </body>
</html>

