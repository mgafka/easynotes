# README #

EasyNotes is an application where you can add you own notes.

# DB SETTINGS #
Edit variables in Db.php
# SQL # 
CREATE table `notes` (
  `id` int not NULL AUTO_INCREMENT PRIMARY KEY,
  `content` VARCHAR(255) NOT NULL,
  `date` VARCHAR(255) NOT NULL
) ENGINE = INNODB;

